<?php

Route::group([
    'prefix' => 'test',
    'namespace' => 'Tests\Controllers',
    ], function () {
        Route::get('testing-url', 'TestPageController@index')->name('testing-url');
    }
);
