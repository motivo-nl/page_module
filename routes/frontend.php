<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;
use Motivo\Liberiser\Pages\Models\Page;
use Motivo\Liberiser\Base\Models\Language;

Route::group(['middleware' => 'web'], function () {
    $activeLanguages = [];
    $locale = App::getLocale();
    $pageController = config('liberiser.pages.page_controller');

    $pageQuery = (isset(Page::getPlugins()['visibility']) && Schema::hasTable('liberiser_visibility'))
        ? Page::visible()
        : Page::query();

    if (Schema::hasTable('liberiser_languages')) {
        $activeLanguages = Language::active();
        $locale = get_user_locale(request());
    }

    // Controller
    if (Schema::hasTable('liberiser_pages') && Schema::hasColumns('liberiser_pages', ['named_route', 'controller'])) {
        $query = clone $pageQuery;

        foreach ($query->whereNotNull('controller')->whereNull('named_route')->get() as $page) {
            $index = strpos($page->controller, '@');
            $controller = substr($page->controller, 0, $index);

            if (class_exists($controller)) {
                /** @var Page $page */
                foreach ($page->parents as $menuItem) {
                    $baseLocale = get_menu_item_base_locale($menuItem, $locale);

                    Route::get($menuItem->getTranslation('link', $baseLocale), $page->controller);

                    foreach ($activeLanguages as $language) {
                        /* @var \Motivo\Liberiser\Base\Models\Language $language */
                        $link = $menuItem->getTranslation('link', $language->shortcode);

                        if ($link && strlen($link) > 1) {
                            Route::get($language->shortcode.$link, $page->controller);
                        }
                    }
                }
            }
        }
    }

    // Named routes
    if (Schema::hasTable('liberiser_pages') && Schema::hasColumns('liberiser_pages', ['named_route', 'controller'])) {
        $query = clone $pageQuery;

        foreach ($query->whereNotNull('named_route')->get() as $page) {
            if ($menuItem = $page->parents()->first()) {
                $controller = $page->controller ?? $pageController.'@namedRoute';
                $namedLocale = get_menu_item_base_locale($menuItem, $locale);

                Route::get($menuItem->getTranslation('link', $namedLocale), $controller)
                    ->name($page->named_route);

                foreach ($activeLanguages as $language) {
                    if (isset($page->visibility) && isset($page->visibility->visible_languages) && ! in_array($language->shortcode, $page->visibleLanguages())) {
                        continue;
                    }

                    if ($link = $menuItem->getTranslation('link', $language->shortcode)) {
                        Route::get("/{$language->shortcode}{$link}", $controller)
                            ->name($page->named_route.'.'.$language->shortcode);
                    }
                }
            }
        }
    }

    // Modules
    if (Schema::hasTable('liberiser_pages') && Schema::hasColumn('liberiser_pages', 'module')) {
        $query = clone $pageQuery;

        foreach ($query->whereNotNull('module')->get() as $page) {
            /** @var Page $page */
            foreach ($page->parents as $menuItem) {
                $baseLocale = get_menu_item_base_locale($menuItem, $locale);

                /** @var \Motivo\Liberiser\Pages\Models\MenuItem $menuItem */
                $routesClass = config("liberiser.{$page->module}.module_routes_class", null);

                if ($routesClass) {
                    $routesClass::registerRoutes($menuItem->getTranslation('link', $baseLocale));

                    foreach ($activeLanguages as $language) {
                        /* @var \Motivo\Liberiser\Base\Models\Language $language */
                        $routesClass::registerRoutes($language->shortcode.$menuItem->getTranslation('link', $language->shortcode));
                    }
                }
            }
        }
    }

    Route::fallback($pageController.'@show');
});
