<?php

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
], function () {
    Route::group(['prefix' => 'page'], function () {
        CRUD::resource('menu', config('liberiser.menu.menu_crud_controller'));
        Route::delete('menu/item/{menuItem}/delete', config('liberiser.menu.menu_crud_controller').'@destroyMenuItem')->name('crud.menu.item.destroy');
        Route::post('menu/update-order', config('liberiser.menu.menu_crud_controller').'@updateOrder')->name('crud.menu.update-order');
    });

    Route::post('page/update-order', config('liberiser.pages.page_crud_controller').'@updateOrder')->name('crud.page.update-order');
    CRUD::resource('page', config('liberiser.pages.page_crud_controller'));
    Route::get('page/create/{parentType}/{parentId}', config('liberiser.pages.page_crud_controller').'@customCreate')->name('crud.page.create');
});
