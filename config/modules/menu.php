<?php

use Motivo\Liberiser\Pages\Models\Menu;
use Motivo\Liberiser\Base\Models\Permission;
use Motivo\Liberiser\Pages\Http\Controllers\MenuCrudController;

return [
    'class' => Menu::class,
    'package_namespace' => 'LiberiserPages',
    'required_permission_level' => Permission::ROLE_ADMIN,
    'menu_crud_controller' => MenuCrudController::class,
];
