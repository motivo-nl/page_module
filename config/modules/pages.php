<?php

use Motivo\Liberiser\Pages\Models\Page;
use Motivo\Liberiser\Base\Models\Permission;
use Motivo\Liberiser\Pages\Http\Controllers\PageController;
use Motivo\Liberiser\Pages\Http\Controllers\PagesCrudController;

return [
    'class' => Page::class,
    'package_namespace' => 'LiberiserPages',
    'required_permission_level' => Permission::ROLE_CONTENT_MANAGER,
    'page_controller' => PageController::class,
    'page_crud_controller' => PagesCrudController::class,
    'seeders' => [
        'Motivo\Liberiser\Pages\Database\Seeds\MenuSeeder',
    ],
];
