<?php

use Illuminate\Http\Request;
use Motivo\Liberiser\Pages\Models\Page;
use Motivo\Liberiser\Base\Models\Language;
use Motivo\Liberiser\Pages\Models\MenuItem;

if (! function_exists('liberiserNamedRoute')) {
    function liberiserNamedRoute($name, $parameters = [], $absolute = true)
    {
        if ($locale = get_locale_from_url(request())) {
            $suffixedName = "{$name}.{$locale}";

            if (Route::has($suffixedName)) {
                return app('url')->route($suffixedName, $parameters, $absolute);
            }
        }

        return app('url')->route($name, $parameters, $absolute);
    }
}

if (! function_exists('get_user_locale')) {
    function get_user_locale(Request $request): string
    {
        if ($request->get('locale')) {
            return $request->get('locale');
        }

        if ($localeFromUrl = get_locale_from_url($request)) {
            return $localeFromUrl;
        }

        if ($localeFromBrowserPreference = get_locale_from_browser_preference($request)) {
            return $localeFromBrowserPreference;
        }

        return app()->getLocale();
    }
}

if (! function_exists('get_locale_from_url')) {
    function get_locale_from_url(Request $request): ?string
    {
        $firstSegment = explode('/', $request->path())[0];

        $language = Language::where('shortcode', '=', $firstSegment)->where('active', '=', true)->first();

        if ($language) {
            return $language->shortcode;
        }

        return null;
    }
}

if (! function_exists('get_locale_from_browser_preference')) {
    function get_locale_from_browser_preference() : ?string
    {
        $segments = explode(';', str_replace(',', ';', \Request::server('HTTP_ACCEPT_LANGUAGE')));

        $regex = '/^[a-z]{2}-[A-Z]{2}$/';
        $locales = preg_grep($regex, $segments);

        foreach ($locales as $locale) {
            $language = Language::where('locale', '=', str_replace('-', '_', $locale))->where('active', '=', true)->first();

            if ($language) {
                return $language->shortcode;
            }
        }

        return null;
    }
}

if (! function_exists('format_link_path')) {
    function format_link_path(string $path): string
    {
        $segments = explode('/', $path);
        $language = Language::where('shortcode', '=', $segments[0])->first();

        if ($language) {
            return implode('/', array_slice($segments, 1));
        }

        return $path;
    }
}

if (! function_exists('get_menu_item_base_link')) {
    function get_menu_item_base_locale(MenuItem $menuItem, string $userLocale): string
    {
        if (menu_item_locale_is_set($menuItem, $userLocale)) {
            return $userLocale;
        }

        if (menu_item_locale_is_set($menuItem, config('app.locale'))) {
            return config('app.locale');
        }

        if (menu_item_locale_is_set($menuItem, config('app.fallback_locale'))) {
            return config('app.fallback_locale');
        }

        return $userLocale;
    }
}

if (! function_exists('menu_item_locale_is_set')) {
    function menu_item_locale_is_set(MenuItem $menuItem, string $locale): bool
    {
        $link = $menuItem->hasTranslation('link', $locale);
        $title = $menuItem->page->hasTranslation('title', $locale);

        if (isset($link) && ! empty($link) && isset($title) && ! empty($title)) {
            return true;
        }

        return false;
    }
}

if (! function_exists('get_page_from_url')) {
    function get_page_from_url(string $url, string $locale): ?Page
    {
        if ($url[0] !== '/') {
            $url = '/'.$url;
        }

        $segments = array_filter(explode('/', $url));
        $firstSegment = array_shift($segments);

        if (Language::where('shortcode', '=', $firstSegment)->first()) {
            $url = '/'.implode('/', $segments);
        }

        $menuItem = MenuItem::where("link->{$locale}", '=', $url)->first();

        if (! $menuItem) {
            $menuItem = MenuItem::where('link->'.config('app.locale'), '=', $url)->first();
        }

        if (! $menuItem) {
            $menuItem = MenuItem::where('link->'.config('app.fallback_locale'), '=', $url)->first();
        }

        if (Page::getPlugins()['visibility'] && ! $menuItem->page->isVisible()) {
            return null;
        }

        return $menuItem ? $menuItem->page : null;
    }
}
