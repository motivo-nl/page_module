<?php

namespace Motivo\Liberiser\Pages\Models;

use Motivo\Liberiser\Base\Liberiser;
use Motivo\Liberiser\Seo\Models\Seo;
use Motivo\Liberiser\Base\Models\BaseModel;
use Motivo\Liberiser\Base\Models\Permission;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Page extends BaseModel
{
    use SoftDeletes;

    protected $table = 'liberiser_pages';

    protected $fillable = ['title', 'content', 'slug', 'module', 'named_route', 'controller'];

    public static $translatableFields = ['title', 'content', 'slug'];

    public static function getModuleName(): string
    {
        return 'pages';
    }

    public static function getComponentFields(?BaseModel $model = null): array
    {
        $options = [];

        $options[''] = ucfirst(trans('LiberiserPages::pages.select_module'));
        foreach (Liberiser::getPageModules() as $module => $label) {
            $config = config("liberiser.{$module}");

            $options[$module] = ucfirst(trans("{$config['package_namespace']}::{$module}.name"));
        }

        $fields = [
            [
                'name' => 'title',
                'type' => 'text',
                'label' => ucfirst(trans('LiberiserPages::pages.fields.title')),

            ],
            [
                'name' => 'content',
                'type' => 'wysiwyg',
                'label' => ucfirst(trans('LiberiserPages::pages.fields.content')),
                'options' => [
                    'allowedContent' => true,
                ],
            ],
            [
                'name' => 'fieldset_admin_pages_start',
                'type' => 'custom_html',
                'value' => '<hr /><h3>Motivo only</h3>',
                'required_permission_level' => Permission::ROLE_ADMIN,
            ],
            [
                'name' => 'named_route',
                'type' => 'text',
                'label' => ucfirst(trans('LiberiserPages::pages.fields.named_route')),
                'required_permission_level' => Permission::ROLE_ADMIN,
            ],
            [
                'name' => 'controller',
                'type' => 'text',
                'label' => ucfirst(trans('LiberiserPages::pages.fields.controller')),
                'required_permission_level' => Permission::ROLE_ADMIN,
            ],
        ];

        if (count($options) > 1) {
            $fields[] = [
                'name' => 'module',
                'type' => 'select_from_array',
                'label' => ucfirst(trans('LiberiserPages::pages.fields.module')),
                'options' => $options,
                'default' => '',
                'required_permission_level' => Permission::ROLE_ADMIN,
            ];
        }

        $fields[] = [
            'name' => 'fieldset_admin_pages_end',
            'type' => 'custom_html',
            'value' => '<hr>',
        ];

        return $fields;
    }

    public static function getComponentColumns(): array
    {
        return [
            ['name' => 'title', 'type' => 'text', 'label' => ucfirst(trans('LiberiserPages::pages.columns.title'))],
            ['name' => 'content', 'type' => 'text', 'label' => ucfirst(trans('LiberiserPages::pages.columns.content'))],
        ];
    }

    public static function getComponentRelations(): array
    {
        return [];
    }

    public function getDisplayNameAttribute(): string
    {
        return $this->title;
    }

    public function parents(): HasMany
    {
        return $this->hasMany(MenuItem::class, 'page_id');
    }
}
