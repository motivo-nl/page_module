<?php

namespace Motivo\Liberiser\Pages\Models;

use Motivo\Liberiser\Base\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Menu extends BaseModel
{
    use SoftDeletes;

    protected $table = 'liberiser_menus';

    protected $fillable = ['name'];

    public static function getModuleName(): string
    {
        return 'menu';
    }

    public static function getComponentFields(?BaseModel $model = null): array
    {
        return [
            ['name' => 'name', 'type' => 'text', 'label' => ucfirst(trans('LiberiserPages::menu.fields.name'))],
        ];
    }

    public static function getComponentColumns(): array
    {
        return [
            ['name' => 'name', 'type' => 'text', 'label' => ucfirst(trans('LiberiserPages::menu.columns.name'))],
        ];
    }

    public static function getComponentRelations(): array
    {
        return [];
    }

    public function menuItems(): HasMany
    {
        return $this->hasMany(MenuItem::class, 'menu_id')->whereNull('parent_id')->orderBy('order', 'ASC');
    }

    public function allChildren(): HasMany
    {
        return $this->hasMany(MenuItem::class, 'menu_id')->orderBy('order', 'ASC');
    }

    public function getDisplayNameAttribute(): string
    {
        return $this->name;
    }
}
