<?php

namespace Motivo\Liberiser\Pages\Models;

use Motivo\Liberiser\Base\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MenuItem extends BaseModel
{
    use SoftDeletes;

    public const PARENT_TYPE_MENU = 0;

    public const PARENT_TYPE_MENU_ITEM = 1;

    protected $table = 'liberiser_menu_items';

    protected $fillable = ['menu_id', 'page_id', 'parent_id', 'order', 'link'];

    public static $translatableFields = ['link'];

    public static function getModuleName(): string
    {
        return 'menu_items';
    }

    public static function getComponentFields(?BaseModel $model = null): array
    {
        return [];
    }

    public static function getComponentColumns(): array
    {
        return [];
    }

    public static function getComponentRelations(): array
    {
        return [];
    }

    public static function getStoreRules(): array
    {
        return [];
    }

    public function page(): BelongsTo
    {
        return $this->belongsTo(Page::class, 'page_id');
    }

    public function menu(): BelongsTo
    {
        return $this->belongsTo(self::class);
    }

    public function parent(): BelongsTo
    {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    public function children(): HasMany
    {
        return $this->hasMany(self::class, 'parent_id', 'id')->orderBy('order');
    }

    public function getDisplayNameAttribute(): string
    {
        return $this->page->title;
    }

    public function updateChildren(string $locale): void
    {
        $baseUrl = $this->getTranslation('link', $locale);

        foreach ($this->children as $child) {
            /* @var MenuItem $child */
            $child->setTranslation('link', $locale, $baseUrl.DIRECTORY_SEPARATOR.$child->page->slug);
            $child->save();

            $child->updateChildren($locale);
        }
    }

    public function delete()
    {
        $this->children()->each(function (self $child) {
            $child->delete();
        });

        if ($this->children()->count() > 0) {
            return false;
        }

        $deleted = parent::delete();

        if ($deleted && $this->page->parents()->count() === 0) {
            $this->page->delete();
        }

        return $deleted;
    }
}
