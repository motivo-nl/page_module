<?php

namespace Motivo\Liberiser\Pages\Events;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;
use Illuminate\Queue\SerializesModels;
use Motivo\Liberiser\Base\Models\BaseModel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class UpdatePageEvent extends Event
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /** @var BaseModel */
    public $oldModel;

    /** @var BaseModel */
    public $model;

    /** @var Request */
    public $request;

    public function __construct(BaseModel $oldModel, BaseModel $model, Request $request)
    {
        $this->oldModel = $oldModel;
        $this->model = $model;
        $this->request = $request;
    }
}
