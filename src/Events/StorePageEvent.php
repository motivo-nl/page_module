<?php

namespace Motivo\Liberiser\Pages\Events;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;
use Illuminate\Queue\SerializesModels;
use Motivo\Liberiser\Base\Models\BaseModel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class StorePageEvent extends Event
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /** @var BaseModel */
    public $model;

    /** @var Request */
    public $request;

    public function __construct(BaseModel $model, Request $request)
    {
        $this->model = $model;
        $this->request = $request;
    }
}
