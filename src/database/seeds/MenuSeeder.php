<?php

namespace Motivo\Liberiser\Pages\Database\Seeds;

use Illuminate\Database\Seeder;
use Motivo\Liberiser\Pages\Models\Menu;

class MenuSeeder extends Seeder
{
    private $menus = [
        [
            'name' => 'Hoofdmenu',
        ],
    ];

    public function run(): void
    {
        foreach ($this->menus as $menu) {
            Menu::firstOrCreate($menu);
        }
    }
}
