<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPagesTableAddNamedRouteField extends Migration
{
    public function up(): void
    {
        Schema::table('liberiser_pages', function (Blueprint $table) {
            $table->string('named_route')->nullable()->default(null)->after('module');
        });
    }

    public function down(): void
    {
        Schema::table('liberiser_pages', function (Blueprint $table) {
            $table->dropColumn('named_route');
        });
    }
}
