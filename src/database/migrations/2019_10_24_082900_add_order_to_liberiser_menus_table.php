<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderToLiberiserMenusTable extends Migration
{
    public function up(): void
    {
        Schema::table('liberiser_menus', function (Blueprint $table) {
            $table->integer('order')->default(0)->after('name');
        });
    }

    public function down(): void
    {
        Schema::table('liberiser_menus', function (Blueprint $table) {
            $table->dropColumn('order');
        });
    }
}
