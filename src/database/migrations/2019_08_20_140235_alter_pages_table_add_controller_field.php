<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPagesTableAddControllerField extends Migration
{
    public function up(): void
    {
        Schema::table('liberiser_pages', function (Blueprint $table) {
            $table->string('controller')->nullable()->default(null)->after('named_route');
        });
    }

    public function down(): void
    {
        Schema::table('liberiser_pages', function (Blueprint $table) {
            $table->dropColumn('controller');
        });
    }
}
