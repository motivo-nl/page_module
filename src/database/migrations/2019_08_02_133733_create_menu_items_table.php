<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuItemsTable extends Migration
{
    public function up(): void
    {
        Schema::create('liberiser_menu_items', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedInteger('page_id');
            $table->unsignedInteger('menu_id')->nullable()->default(null);
            $table->unsignedInteger('parent_id')->nullable()->default(null);
            $table->json('link');
            $table->integer('order');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('liberiser_menu_items');
    }
}
