<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    public function up(): void
    {
        Schema::create('liberiser_pages', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->json('slug');
            $table->json('title');
            $table->json('content');
            $table->string('module')->nullable()->default(null);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('liberiser_pages');
    }
}
