<?php

namespace Motivo\Liberiser\Pages;

use Illuminate\Support\Facades\View;
use Motivo\Liberiser\Base\Liberiser;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;
use Motivo\Liberiser\Base\BaseMenuItem;
use Motivo\Liberiser\Pages\Models\Menu;
use Motivo\Liberiser\Pages\Models\Page;
use Motivo\Liberiser\Pages\Http\ViewComposers\PagesIndexComposer;

class PagesServiceProvider extends ServiceProvider
{
    /** @var string */
    protected $publishablePrefix = 'liberiser';

    /** @var string */
    protected $packageNamespace = 'LiberiserPages';

    public function register(): void
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', $this->packageNamespace);

        $this->loadMigrationsFrom(__DIR__.'/database/migrations');

        $this->mergeConfigFrom(__DIR__.'/../config/main/config.php', 'liberiser.config');
        $this->mergeConfigFrom(__DIR__.'/../config/modules/pages.php', 'liberiser.pages');
        $this->mergeConfigFrom(__DIR__.'/../config/modules/menu.php', 'liberiser.menu');
        $this->mergeConfigFrom(__DIR__.'/../config/modules/menu_items.php', 'liberiser.menu_items');

        View::composer('LiberiserPages::pages.index', PagesIndexComposer::class);

        require_once __DIR__.'/helpers.php';
    }

    public function boot(): void
    {
        $this->registerPublishing();

        if ($this->isEnabled()) {
            $this->enabledBoot();
        }
    }

    public function enabledBoot(): void
    {
        $this->registerRoutes();

        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', $this->packageNamespace);

        $this->setMenuItems();

        $this->setPageModules();
    }

    public function registerRoutes(): void
    {
        if (app()->runningUnitTests()) {
            $this->loadRoutesFrom(__DIR__.'/../routes/testing.php');
        }

        $this->loadRoutesFrom(__DIR__.'/../routes/admin.php');
        $this->loadRoutesFrom(__DIR__.'/../routes/frontend.php');
    }

    private function registerPublishing(): void
    {
        $this->publishes([
            __DIR__.'/../config/modules' => config_path('liberiser'),
        ], $this->publishablePrefix.':config');

        $this->publishes([
            __DIR__.'/../config/main' => config_path('liberiser'),
        ], $this->publishablePrefix.':config-main');

        $this->publishes([
            __DIR__.'/../resources/views/pages/' => resource_path('views/vendor/LiberiserPages/pages'),
        ], $this->publishablePrefix.':views');

        $this->publishes([
            __DIR__.'/../resources/views/vendor/backpack/' => resource_path('views/vendor/backpack'),
        ], $this->publishablePrefix.':backpack');

        $this->publishes([
            __DIR__.'/../dist/public' => public_path(),
        ], $this->publishablePrefix.':backpack');
    }

    private function setMenuItems(): void
    {
        if (! $this->app->runningInConsole()) {
            $menu = $this->getBaseMenuItem('fa-folder', 'LiberiserPages::menu.menu_label', route('crud.menu.index'), Menu::getModuleName());
            $page = $this->getBaseMenuItem('fa-file', 'LiberiserPages::pages.menu_label', route('crud.page.index'), Page::getModuleName());
            $page->setChilds($page, $menu);

            Liberiser::setAdminMenuItems(BaseMenuItem::CATEGORY_MODULES, ucfirst(trans('Liberiser::common.modules')), $page);
            Liberiser::setAdminMenuItemOrder(BaseMenuItem::CATEGORY_MODULES, 0);
        }
    }

    private function setPageModules(): void
    {
        Liberiser::addPageModule(Menu::getModuleName(), ucfirst(trans('LiberiserPages::menu.menu_label')));
        Liberiser::addPageModule(Page::getModuleName(), ucfirst(trans('LiberiserPages::pages.menu_label')));
    }

    private function getBaseMenuItem(string $icon, string $label, string $link, string $module): BaseMenuItem
    {
        $menu = new BaseMenuItem();

        $menu->setIcon($icon);
        $menu->setLabel(ucfirst(trans($label)));
        $menu->setLink($link);
        $menu->setModule($module);

        return $menu;
    }

    private function isEnabled(): bool
    {
        $config = Page::getConfig();

        if (isset($config['active']) && $config['active'] === false) {
            return false;
        }

        return true;
    }
}
