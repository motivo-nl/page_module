<?php

namespace Motivo\Liberiser\Pages\Http\Requests;

use Motivo\Liberiser\Pages\Models\Menu;
use Motivo\Liberiser\Base\Http\Requests\LiberiserRequest;
use Motivo\Liberiser\Base\Http\Requests\StoreLiberiserRequest;

class StoreMenuItemRequest extends StoreLiberiserRequest
{
    protected $model = Menu::class;

    public static function getRules(LiberiserRequest $request): array
    {
        return [
            'name' => [
                'required',
                'min:3',
            ],
        ];
    }
}
