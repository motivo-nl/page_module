<?php

namespace Motivo\Liberiser\Pages\Http\Requests;

use Motivo\Liberiser\Pages\Models\Page;
use Motivo\Liberiser\Base\Http\Requests\LiberiserRequest;
use Motivo\Liberiser\Base\Http\Requests\StoreLiberiserRequest;

class StorePagesRequest extends StoreLiberiserRequest
{
    protected $model = Page::class;

    public static function getRules(LiberiserRequest $request): array
    {
        return [
            'title' => [
                'required',
                'min:3',
            ],
            'content' => [
                'nullable',
            ],
            'named_route' => [
                'nullable',
                'unique:liberiser_pages,named_route',
            ],
            'controller' => [
                'nullable',
                'regex:/^[a-zA-Z\\\\\/\\_\\-]+@[a-zA-Z\\_\\-]+$/i',
            ],
        ];
    }
}
