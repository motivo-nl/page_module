<?php

namespace Motivo\Liberiser\Pages\Http\Requests;

use Illuminate\Support\Facades\Request;
use Motivo\Liberiser\Pages\Models\Page;
use Motivo\Liberiser\Base\Models\BaseModel;
use Motivo\Liberiser\Base\Http\Requests\LiberiserRequest;
use Motivo\Liberiser\Base\Http\Requests\UpdateLiberiserRequest;

class UpdatePagesRequest extends UpdateLiberiserRequest
{
    protected $model = Page::class;

    public function __construct(array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $files = [], array $server = [], $content = null)
    {
        $page = Request::route('page');

        if (is_int($page) || is_string($page)) {
            $page = Page::find($page);
        }

        $this->entity = $page;

        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
    }

    public static function getRules(LiberiserRequest $request, ?BaseModel $entity): array
    {
        $id = $entity ? $entity->id : 0;

        return [
            'title' => [
                'required',
                'min:3',
            ],
            'content' => [
                'nullable',
            ],
            'named_route' => [
                'nullable',
                'unique:liberiser_pages,named_route,'.$id,
            ],
            'controller' => [
                'nullable',
                'regex:/^[a-zA-Z\\\\\/\\_\\-]+@[a-zA-Z\\_ \\-]+$/i',
            ],
        ];
    }
}
