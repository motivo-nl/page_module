<?php

namespace Motivo\Liberiser\Pages\Http\Requests;

use Illuminate\Support\Facades\Request;
use Motivo\Liberiser\Pages\Models\Menu;
use Motivo\Liberiser\Base\Models\BaseModel;
use Motivo\Liberiser\Base\Http\Requests\LiberiserRequest;
use Motivo\Liberiser\Base\Http\Requests\UpdateLiberiserRequest;

class UpdateMenuItemRequest extends UpdateLiberiserRequest
{
    protected $model = Menu::class;

    public function __construct(array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $files = [], array $server = [], $content = null)
    {
        $menu = Request::route('menu');

        if (is_int($menu) || is_string($menu)) {
            $menu = Menu::find($menu);
        }

        $this->entity = $menu;

        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
    }

    public static function getRules(LiberiserRequest $request, ?BaseModel $entity): array
    {
        return [
            'name' => [
                'required',
                'min:3',
            ],
        ];
    }
}
