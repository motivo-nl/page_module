<?php

namespace Motivo\Liberiser\Pages\Http\Controllers;

use Alert;
use Illuminate\Http\Request;
use Motivo\Liberiser\Pages\Models\Menu;
use Motivo\Liberiser\Pages\Models\MenuItem;
use Motivo\Liberiser\Pages\Http\Requests\StoreMenuItemRequest;
use Motivo\Liberiser\Pages\Http\Requests\UpdateMenuItemRequest;
use Motivo\Liberiser\Base\Http\Controllers\LiberiserCrudController;

class MenuCrudController extends LiberiserCrudController
{
    protected $model = Menu::class;

    protected $storeRequest = StoreMenuItemRequest::class;

    protected $updateRequest = UpdateMenuItemRequest::class;

    protected $crudRoute = '/page/menu';

    protected $entityNameSingular = 'LiberiserPages::menu.singular';

    protected $entityNamePlural = 'LiberiserPages::menu.plural';

    public function setup(): void
    {
        parent::setup();

        $this->crud->addButtonFromView('line', 'update_menu', 'update_item', 'end');
        $this->crud->addButtonFromView('line', 'reorder_menu', 'reorder_page', 'end');
        $this->crud->addButtonFromView('line', 'delete_menu', 'delete_menu', 'end');

        $this->crud->macro('getMenus', function () {
            return Menu::orderBy('order')->get();
        });

        $this->crud->setListView('LiberiserPages::menus.index');

        $this->crud->allowAccess('show');
        $this->crud->allowAccess('create');

        $this->crud->removeButtonFromStack('show', 'line');
    }

    public function store(StoreMenuItemRequest $request)
    {
        return parent::processStore($request);
    }

    public function update(UpdateMenuItemRequest $request)
    {
        return parent::processUpdate($request);
    }

    public function destroyMenuItem(MenuItem $menuItem): string
    {
        return $menuItem->delete();
    }

    public function updateOrder(Request $request): void
    {
        $this->updateMenuOrder(json_decode($request->input('order'), true));

        Alert::success(ucfirst(trans('LiberiserPages::pages.updated_order')))->flash();
    }

    private function updateMenuOrder(array $menuItems): void
    {
        foreach ($menuItems as $menuId => $menuItem) {
            $menu = Menu::find($menuId);

            $menu->order = $menuItem['order'];

            $menu->save();
        }
    }
}
