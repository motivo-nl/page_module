<?php

namespace Motivo\Liberiser\Pages\Http\Controllers;

use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Motivo\Liberiser\Pages\Models\Page;
use Motivo\Liberiser\Base\Models\Language;
use Motivo\Liberiser\Pages\Models\MenuItem;
use Motivo\Liberiser\Redirects\Models\Redirect;
use Motivo\Liberiser\Base\Http\Controllers\Controller;

class PageController extends Controller
{
    private $model;

    public function __construct()
    {
        $this->model = Page::getActualClass();
    }

    public function show(Request $request)
    {
        $locale = get_user_locale($request);
        App::setLocale($locale);

        $path = $this->formatPath($request->path(), false);

        if ($to = $this->hasRedirect($request->path(), $locale)) {
            return redirect($to);
        }

        $path = $this->formatPath($request->path(), true);

        $item = MenuItem::where('link->'.$locale, $path)->firstOrFail();

        $this->checkVisibility($item);

        $page = $this->model::find($item->page_id);

        return view('LiberiserPages::pages.show')->with('page', $page);
    }

    public function namedRoute(Request $request): View
    {
        $namedRoute = $request->route()->action['as'];

        $segments = explode('.', $namedRoute);
        $language = Language::where('shortcode', '=', end($segments))->first();

        if ($language) {
            $namedRoute = implode('.', array_slice($segments, 0, count($segments) - 1));
        }

        $page = $this->model::where('named_route', '=', $namedRoute)->firstOrFail();

        $menuItem = $page->parents()->first();

        $this->checkVisibility($menuItem);

        $locale = get_menu_item_base_locale($menuItem, get_user_locale($request));
        App::setLocale($locale);

        return view('LiberiserPages::pages.show')->with('page', $page);
    }

    private function checkVisibility(MenuItem $item): void
    {
        abort_if(isset($this->model::getPlugins()['visibility']) && ! $item->page->isVisible(), Response::HTTP_NOT_FOUND);
    }

    private function hasRedirect(string $path): ?string
    {
        $sanitizedPath = $this->formatPath($path, false);

        $redirect = Redirect::where(['from' => $sanitizedPath])->first();

        if ($redirect) {
            if ($sanitizedPath !== DIRECTORY_SEPARATOR.$path) {
                return '/'.explode('/', $path)[0].$redirect->to;
            }

            return $redirect->to;
        }

        return null;
    }

    private function formatPath(string $path, bool $filterLanguage = true): string
    {
        $segments = explode('/', ltrim($path, '/'));
        $language = Language::where('shortcode', '=', $segments[0])->first();

        if ($language && Language::where('active', 1)->count() > 1 && $filterLanguage) {
            return '/'.implode('/', array_slice($segments, 1));
        }

        return '/'.$path;
    }
}
