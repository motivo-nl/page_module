<?php

namespace Motivo\Liberiser\Pages\Http\Controllers;

use Alert;
use Illuminate\View\View;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Motivo\Liberiser\Pages\Models\Menu;
use Motivo\Liberiser\Pages\Models\Page;
use Motivo\Liberiser\Base\Models\Language;
use Motivo\Liberiser\Pages\Models\MenuItem;
use Motivo\Liberiser\Redirects\Models\Redirect;
use Motivo\Liberiser\Pages\Events\StorePageEvent;
use Motivo\Liberiser\Pages\Events\UpdatePageEvent;
use Motivo\Liberiser\Pages\Http\Requests\StorePagesRequest;
use Motivo\Liberiser\Pages\Http\Requests\UpdatePagesRequest;
use Motivo\Liberiser\Base\Http\Controllers\LiberiserCrudController;

class PagesCrudController extends LiberiserCrudController
{
    protected $model = Page::class;

    protected $storeRequest = StorePagesRequest::class;

    protected $updateRequest = UpdatePagesRequest::class;

    protected $crudRoute = '/page';

    protected $entityNameSingular = 'LiberiserPages::pages.singular';

    protected $entityNamePlural = 'LiberiserPages::pages.plural';

    public function setup(): void
    {
        parent::setup();

        $this->crud->addButtonFromView('line', 'reorder_page', 'reorder_page', 'end');
        $this->crud->addButtonFromView('line', 'add', 'add_page', 'end');
        $this->crud->addButtonFromView('line', 'update_page', 'update_page', 'end');
        $this->crud->addButtonFromView('line', 'show_page', 'show_page', 'end');
        $this->crud->addButtonFromView('line', 'delete_page', 'delete_page', 'end');

        $this->crud->allowAccess('show');
        $this->crud->removeButtonFromStack('show', 'line');

        $this->crud->setListView('LiberiserPages::pages.index');
        $this->crud->setPreviewView('LiberiserPages::pages.preview');
    }

    public function customCreate($parentType, $parentId): View
    {
        $this->crud->addFields([
            [
                'name' => 'parent_type',
                'type' => 'hidden',
                'value' => $parentType,
            ],
            [
                'name' => 'parent_id',
                'type' => 'hidden',
                'value' => $parentId,
            ],
        ]);

        return $this->create();
    }

    public function store(StorePagesRequest $request)
    {
        $locale = $request->input('locale', App::getLocale());

        $parentType = $request->input('parent_type', MenuItem::PARENT_TYPE_MENU);
        $parentId = $request->input('parent_id', null);
        $slug = Str::slug($request->input('title'));

        $link = $parentType == 1 ? (MenuItem::find($request->input('parent_id'))->link.DIRECTORY_SEPARATOR.$slug) : DIRECTORY_SEPARATOR.$slug;
        $order = $parentType == 0 ? Menu::find($request->input('parent_id'))->menuItems()->count() : MenuItem::find($request->input('parent_id'))->children()->count();

        $request->merge([
            'slug' => $slug,
        ]);

        $store = parent::processStore($request);

        $parent = $parentType == 1 ? MenuItem::find($parentId) : null;
        $menu = $parentType == 0 ? Menu::find($parentId) : $parent->menu;

        $item = new MenuItem();
        $item->page()->associate($this->crud->entry);
        $item->menu()->associate($menu);
        $item->parent()->associate($parent);
        $item->setTranslation('link', $locale, $link);
        $item->order = $order;
        $item->save();

        $redirect = Redirect::where('from', '=', $link)->first();

        if ($redirect) {
            $redirect->delete();
        }

        return $store;
    }

    public function update(UpdatePagesRequest $request, Page $page)
    {
        $locale = $request->input('locale', App::getLocale());

        $originalSlug = $page->slug;
        $newSlug = Str::slug($request->input('title'));

        $request->merge([
            'slug' => $newSlug,
        ]);

        $update = parent::processUpdate($request);

        if ($originalSlug !== $newSlug) {
            $this->updatePageLinks($this->crud->entry, $locale);
        }

        return $update;
    }

    public function updateOrder(Request $request): void
    {
        $this->updateMenuOrder(json_decode($request->input('order'), true));

        Alert::success(ucfirst(trans('LiberiserPages::pages.updated_order')))->flash();
    }

    private function updateMenuOrder(array $mainMenu): void
    {
        foreach ($mainMenu as $menuId => $menuItems) {
            foreach ($menuItems['children'] as $menuItemId => $children) {
                /** @var MenuItem $item */
                $item = MenuItem::find($menuItemId);
                $menu = Menu::find($menuId);

                $item->parent()->dissociate();
                $item->menu()->associate($menu);
                $item->order = $children['order'];

                foreach (Language::active() as $language) {
                    $locale = $language->shortcode;
                    $item->setTranslation('link', $locale, DIRECTORY_SEPARATOR.$item->page->getTranslation('slug', $locale));
                }

                $item->save();

                $this->updateItemOrder($menu, $item, $children);
            }
        }
    }

    private function updateItemOrder(Menu $menu, MenuItem $parent, array $items): void
    {
        foreach ($items['children'] as $itemId => $children) {
            /** @var MenuItem $item */
            $item = MenuItem::find($itemId);

            $item->menu()->associate($menu);
            $item->parent()->associate($parent);
            $item->order = $children['order'];

            foreach (Language::active() as $language) {
                $locale = $language->shortcode;
                $link = $parent->getTranslation('link', $locale).DIRECTORY_SEPARATOR.$item->page->getTranslation('slug', $locale);

                $item->setTranslation('link', $locale, $link);
            }

            $item->save();

            $this->updateItemOrder($menu, $item, $children);
        }
    }

    private function updatePageLinks(Page $page, string $locale): void
    {
        foreach ($page->parents as $parent) {
            /** @var MenuItem $parent */
            $url = $parent->getTranslation('link', $locale);

            if (strlen($url) === 0) {
                $url = DIRECTORY_SEPARATOR;
            }

            $index = strrpos($url, DIRECTORY_SEPARATOR);
            $newLink = substr($url, 0, $index + 1).$parent->page->getTranslation('slug', $locale);
            $oldLink = $parent->getTranslation('link', $locale);

            $parent->setTranslation('link', $locale, $newLink);
            $parent->save();

            if (isset($oldLink) && ! empty($oldLink)) {
                if (Language::where('active', 1)->count() > 1) {
                    $newLink = DIRECTORY_SEPARATOR.$locale.$newLink;
                    $oldLink = DIRECTORY_SEPARATOR.$locale.$oldLink;
                }

                $redirect = Redirect::where('from', '=', $newLink)->first();

                if ($redirect) {
                    $redirect->delete();
                }

                Redirect::where('to', '=', $oldLink)->update(['to' => $newLink, 'type' => Redirect::TYPE_GENERATED]);
                Redirect::create(['from' => $oldLink, 'to' => $newLink, 'type' => Redirect::TYPE_GENERATED]);
            }

            $parent->updateChildren($locale);
        }
    }
}
