<?php

namespace Motivo\Liberiser\Pages\Http\Controllers;

use Motivo\Liberiser\Pages\Models\Page;
use Motivo\Liberiser\Base\Http\Controllers\Controller as BaseController;

abstract class Controller extends BaseController
{
    /** @var null|Page */
    protected $page = null;

    /** @var null|string */
    protected $locale = null;

    public function __construct()
    {
        $request = request();

        $this->locale = get_user_locale($request);

        if (! empty($request->route())) {
            $this->page = get_page_from_url($request->route()->uri, $this->locale);
        }
    }
}
