<?php

namespace Motivo\Liberiser\Pages\Http\ViewComposers;

use Illuminate\View\View;
use Motivo\Liberiser\Pages\Models\Menu;

class PagesIndexComposer
{
    public function compose(View $view): void
    {
        $view->with('menuItems', Menu::orderBy('order')->get());
    }
}
