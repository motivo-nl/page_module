<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Http\Response;
use App\Http\Middleware\CheckIfAdmin;
use Motivo\Liberiser\Base\Models\User;
use App\Http\Middleware\VerifyCsrfToken;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MenuTest extends TestCase
{
    use DatabaseTransactions;
    use WithFaker;

    /** @test */
    public function can_create_menu_test(): void
    {
        $inputData = [
            'name' => $this->faker->name,
        ];

        $response = $this->post(route('crud.menu.store'), $inputData);

        $response->assertStatus(Response::HTTP_FOUND);

        $this->assertDatabaseHas('liberiser_menus', ['name' => $inputData['name']]);
    }

    /** @test */
    public function can_edit_menu_test(): void
    {
        $inputData = [
            'name' => $this->faker->name,
        ];

        $updateData = [
            'id' => 1,
            'name' => $this->faker->name,
        ];

        $this->post(route('crud.menu.store'), $inputData)->assertStatus(Response::HTTP_FOUND);
        $this->put(route('crud.menu.update', ['menu' => 1]), $updateData)->assertStatus(Response::HTTP_FOUND);

        $this->assertDatabaseMissing('liberiser_menus', ['name' => $inputData['name']]);
        $this->assertDatabaseHas('liberiser_menus', ['name' => $updateData['name']]);
    }

    /** @test */
    public function can_delete_menu_test(): void
    {
        $inputData = [
            'name' => $this->faker->name,
        ];

        $this->post(route('crud.menu.store'), $inputData)->assertStatus(Response::HTTP_FOUND);
        $this->delete(route('crud.menu.destroy', ['menu' => 1]))->assertStatus(Response::HTTP_OK);

        $this->assertSoftDeleted('liberiser_menus', ['name' => $inputData['name']]);
    }
}
