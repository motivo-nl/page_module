<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Route;
use Motivo\Liberiser\Pages\Models\Page;
use Motivo\Liberiser\Pages\Models\MenuItem;
use Illuminate\Foundation\Testing\WithFaker;
use Motivo\Liberiser\Base\Models\Permission;

class NamedRoutesTest extends TestCase
{
    use WithFaker;

    public function setUp(): void
    {
        parent::setUp();

        $this->be(new User([
            'id' => 1,
            'first_name' => 'User',
            'last_name' => 'Name',
            'role' => Permission::ROLE_ADMIN,
        ]));

        $this->artisan('db:seed', ['--class' => 'Motivo\\Liberiser\\Base\\Database\\Seeds\\LanguageSeeder']);
    }

    /** @test */
    public function named_routes_do_exist(): void
    {
        $locale = 'nl';

        $rootData = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $locale,
            'named_route' => 'named.route',
        ];

        $this->createMenu();
        $this->createPage(MenuItem::PARENT_TYPE_MENU, 1, $rootData);

        $this->app = $this->createApplication();
        $this->assertTrue(Route::has($rootData['named_route']));
        $this->assertTrue(Route::has("{$rootData['named_route']}.{$locale}"));
        $this->assertFalse(Route::has("{$rootData['named_route']}.nonlanguage"));
    }

    /** @test */
    public function named_route_with_controller_test(): void
    {
        $locale = 'nl';

        $rootData = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $locale,
            'named_route' => 'testing-url',
            'controller' => 'Tests\Controllers\TestPageController@index',
        ];

        $this->createMenu();
        $this->createPage(MenuItem::PARENT_TYPE_MENU, 1, $rootData);

        $this->app = $this->createApplication();
        app()->setLocale($locale);

        $this->get(route($rootData['named_route']))
            ->assertOk()
            ->assertSeeText($rootData['title']);
    }

    /** @test */
    public function named_routes_work_for_multiple_languages(): void
    {
        $localeNl = 'nl';
        $localeEn = 'en';

        $data = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $localeNl,
            'named_route' => 'testing-url',
            'controller' => 'Tests\Controllers\TestPageController@index',
        ];

        $dataEn = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $localeEn,
        ];

        $this->createMenu();
        $page = $this->createPage(MenuItem::PARENT_TYPE_MENU, 1, $data);
        $this->updatePage($page, $dataEn);

        $this->app = $this->createApplication();

        $this->assertTrue(Route::has($data['named_route']));
        $this->assertTrue(Route::has("{$data['named_route']}.{$localeNl}"));
        $this->assertTrue(Route::has("{$data['named_route']}.{$localeEn}"));

        $urlRoot = config('app.url');

        $this->get($page->parents()->first()->getTranslation('link', $localeEn));
        $this->assertEquals(liberiserNamedRoute($data['named_route']), "{$urlRoot}/{$page->slug}");

        $this->get("{$localeEn}/{$page->parents()->first()->getTranslation('link', $localeEn)}");
        $this->assertEquals(liberiserNamedRoute($data['named_route']), "{$urlRoot}/{$localeEn}/{$page->getTranslation('slug', $localeEn)}");

        $this->get(liberiserNamedRoute($data['named_route']))->assertOk();
    }

    private function createPage(int $parentType, int $parentId, array $data): Page
    {
        $data = array_merge($data, ['parent_type' => $parentType, 'parent_id' => $parentId]);

        $this->post(route('crud.page.store'), $data)->assertStatus(Response::HTTP_FOUND);

        return Page::orderby('id', 'DESC')->first();
    }

    private function updatePage(Page $page, array $data): Page
    {
        $data = array_merge($data, ['id' => $page->id]);

        $this->put(route('crud.page.update', ['page' => 1]), $data)->assertStatus(Response::HTTP_FOUND);

        return $page->refresh();
    }

    private function createMenu(int $amount = 1): void
    {
        for ($i = 0; $i < $amount; $i++) {
            $inputData = [
                'name' => $this->faker->name,
            ];

            $this->post(route('crud.menu.store'), $inputData)->assertStatus(Response::HTTP_FOUND);
        }
    }
}
