<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\Route;
use Motivo\Liberiser\Pages\Models\Menu;
use Motivo\Liberiser\Pages\Models\Page;
use Motivo\Liberiser\Pages\Models\MenuItem;
use Illuminate\Foundation\Testing\WithFaker;

class ExtendedControllerTest extends TestCase
{
    use WithFaker;

    /** @var string */
    private const TEST_CONTROLLER_NAMESPACE = 'Tests\Controllers\TestPageController@index';

    /** @var int */
    private const PAGE_ORDER = 1;

    /** @var string */
    private const LOCALE_EN = 'en';

    /** @var Page */
    protected $page;

    /** @var Menu */
    protected $menu;

    public function setUp(): void
    {
        parent::setUp();

        $this->page = factory(Page::class)->create([
            'slug' => json_encode([self::LOCALE_EN => $this->faker->slug]),
            'title' => json_encode([self::LOCALE_EN => $this->faker->name]),
            'content' => json_encode([self::LOCALE_EN => $this->faker->text]),
            'named_route' => 'testing-url',
            'controller' => self::TEST_CONTROLLER_NAMESPACE,
        ]);

        $this->menu = factory(Menu::class)->create();

        $menuItem = MenuItem::create([
            'page_id' => $this->page->id,
            'menu_id' => $this->menu->id,
            'link' => '/testing-url',
            'order' => self::PAGE_ORDER,
        ]);

        $menuItem->page()->associate($this->page);
        $menuItem->menu()->associate($this->menu);
        $menuItem->save();
    }

    /** @test */
    public function controller_field_of_page(): void
    {
        $namedRoute = $this->page->named_route;

        $this->assertTrue(Route::has($namedRoute));

        $this->get(route($namedRoute))
            ->assertOk()
            ->assertSeeText($this->page->title);
    }
}
