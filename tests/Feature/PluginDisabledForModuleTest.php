<?php

namespace Tests\Feature;

use Tests\TestCase;
use BadMethodCallException;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Config;
use Motivo\Liberiser\Pages\Models\Menu;
use Motivo\Liberiser\Pages\Models\Page;
use Motivo\Liberiser\Pages\Models\MenuItem;
use Illuminate\Foundation\Testing\WithFaker;

class PluginDisabledForModuleTest extends TestCase
{
    use WithFaker;

    /** @var Page */
    private $page;

    /** @var Menu */
    private $menu;

    /** @var string */
    private const TEST_CONTROLLER_NAMESPACE = 'Tests\Controllers\TestPageController@index';

    /** @var int */
    private const PAGE_ORDER = 1;

    /** @var string */
    private const LOCALE_EN = 'en';

    protected function getEnvironmentSetUp($app): void
    {
        $config = $app->get('config');
        $config->set('liberiser.config.modules.pages.plugins', []);

        parent::getEnvironmentSetUp($app);
    }

    public function setUp(): void
    {
        parent::setUp();

        $this->page = factory(Page::class)->create([
            'slug' => json_encode([self::LOCALE_EN => $this->faker->slug]),
            'title' => json_encode([self::LOCALE_EN => $this->faker->name]),
            'content' => json_encode([self::LOCALE_EN => $this->faker->text]),
            'named_route' => 'testing-url',
            'controller' => self::TEST_CONTROLLER_NAMESPACE,
        ]);

        $this->menu = factory(Menu::class)->create();

        $menuItem = MenuItem::create([
            'page_id' => $this->page->id,
            'menu_id' => $this->menu->id,
            'link' => '/testing-url',
            'order' => self::PAGE_ORDER,
        ]);

        $menuItem->page()->associate($this->page);
        $menuItem->menu()->associate($this->menu);
        $menuItem->save();
    }

    /** @test */
    public function seo_plugin_is_disabled_for_module(): void
    {
        $this->expectException(BadMethodCallException::class);
        $this->expectExceptionMessage('Call to undefined method Motivo\Liberiser\Pages\Models\Page::seo()');

        Page::getConfig(true);

        $title = json_encode([self::LOCALE_EN => $this->faker->title]);
        $text = json_encode([self::LOCALE_EN => $this->faker->text]);

        $this->page->seo()->create([
            'seo_title' => $title,
            'seo_description' => $text,
        ]);
    }
}
