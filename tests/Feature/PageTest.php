<?php

namespace Tests\Feature;

use Str;
use App\User;
use Tests\TestCase;
use Illuminate\Http\Response;
use App\Http\Middleware\CheckIfAdmin;
use Motivo\Liberiser\Pages\Models\Page;
use App\Http\Middleware\VerifyCsrfToken;
use Illuminate\Support\Str as StrHelper;
use Motivo\Liberiser\Pages\Models\MenuItem;
use Illuminate\Foundation\Testing\WithFaker;
use Motivo\Liberiser\Base\Models\Permission;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PageTest extends TestCase
{
    use DatabaseTransactions;
    use WithFaker;

    public function setUp(): void
    {
        parent::setUp();

        $this->be(new User([
            'id' => 1,
            'first_name' => 'User',
            'last_name' => 'Name',
            'role' => Permission::ROLE_ADMIN,
        ]));

        $this->artisan('db:seed', ['--class' => 'Motivo\\Liberiser\\Base\\Database\\Seeds\\LanguageSeeder']);
    }

    /** @test */
    public function create_page_test(): void
    {
        $locale = 'nl';

        $inputData = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $locale,
        ];

        $this->createMenu();
        $this->createPage(MenuItem::PARENT_TYPE_MENU, 1, $inputData);

        $link = DIRECTORY_SEPARATOR.Str::slug($inputData['title']);

        $this->assertDatabaseHas('liberiser_pages', ["title->{$locale}" => $inputData['title']]);
        $this->assertDatabaseHas('liberiser_menu_items', ["link->{$locale}" => $link]);
    }

    /** @test */
    public function update_page_test(): void
    {
        $locale = 'nl';

        $inputData = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $locale,
        ];

        $updateData = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $locale,
        ];

        $this->createMenu();
        $page = $this->createPage(MenuItem::PARENT_TYPE_MENU, 1, $inputData);
        $page = $this->updatePage($page, $updateData);

        $link = DIRECTORY_SEPARATOR.$page->getTranslation('slug', $locale);

        $this->assertDatabaseMissing('liberiser_pages', ["title->{$locale}" => $inputData['title']]);
        $this->assertDatabaseHas('liberiser_pages', ["title->{$locale}" => $updateData['title']]);
        $this->assertDatabaseHas('liberiser_menu_items', ["link->{$locale}" => $link]);
    }

    /** @test */
    public function nested_page_test(): void
    {
        $locale = 'nl';

        $rootData = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $locale,
        ];

        $childData = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $locale,
        ];

        $this->createMenu();
        $page = $this->createPage(MenuItem::PARENT_TYPE_MENU, 1, $rootData);

        foreach ($page->parents as $parent) {
            $this->assertNull($parent->parent);
            $this->assertNotNull($parent->menu);
        }

        $page2 = $this->createPage(MenuItem::PARENT_TYPE_MENU_ITEM, 1, $childData);

        foreach ($page2->parents as $parent) {
            $this->assertNotNull($parent->menu);
            $this->assertNotNull($parent->parent);
            $this->assertEquals($parent->parent->page->id, $page->id);
        }

        $this->assertDatabaseHas('liberiser_pages', ["title->{$locale}" => $rootData['title']]);
        $this->assertDatabaseHas('liberiser_pages', ["title->{$locale}" => $childData['title']]);
    }

    /** @test */
    public function multilanguage_page_test(): void
    {
        $nl = 'nl';
        $en = 'en';

        $nlData = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $nl,
        ];

        $enData = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $en,
        ];

        $this->createMenu();
        $page = $this->createPage(MenuItem::PARENT_TYPE_MENU, 1, $nlData);
        $this->updatePage($page, $enData);

        $this->assertDatabaseHas('liberiser_pages', ["title->{$nl}" => $nlData['title']]);
        $this->assertDatabaseHas('liberiser_pages', ["title->{$en}" => $enData['title']]);
        $this->assertDatabaseMissing('liberiser_pages', ["title->{$nl}" => $enData['title']]);
        $this->assertDatabaseMissing('liberiser_pages', ["title->{$en}" => $nlData['title']]);

        $updateEnData = [
            'title' => $this->faker->name.'-redirect',
            'locale' => $en,
        ];

        $this->updatePage($page, $updateEnData);
        $this->assertDatabaseMissing('liberiser_pages', ["title->{$en}" => $enData['title']]);
        $this->assertDatabaseHas('liberiser_redirects', ['from' => '/'.$en.'/'.StrHelper::slug($enData['title'])]);
        $this->assertDatabaseHas('liberiser_redirects', ['to' => '/'.$en.'/'.StrHelper::slug($updateEnData['title'])]);
    }

    /** @test */
    public function change_order_page_test(): void
    {
        $locale = 'nl';

        $this->createMenu();

        $pageData = [];

        for ($i = 0; $i < 3; $i++) {
            $pageData[$i] = [
                'title' => $this->faker->name,
                'content' => $this->faker->paragraph,
                'locale' => $locale,
            ];

            $this->createPage(MenuItem::PARENT_TYPE_MENU, 1, $pageData[$i]);
        }

        $orderData = [
            'order' => json_encode([
               '1' => [
                   'order' => 1,
                   'children' => [
                       '1' => [
                           'order' => '1',
                           'children' => [],
                       ],
                       '2' => [
                           'order' => '2',
                           'children' => [],
                       ],
                       '3' => [
                           'order' => '0',
                           'children' => [],
                       ],
                   ],
               ],
            ]),
        ];

        $this->post(route('crud.page.update-order'), $orderData);

        $this->assertDatabaseHas('liberiser_menu_items', ['id' => 1, 'order' => 1]);
        $this->assertDatabaseHas('liberiser_menu_items', ['id' => 2, 'order' => 2]);
        $this->assertDatabaseHas('liberiser_menu_items', ['id' => 3, 'order' => 0]);
    }

    /** @test */
    public function change_order_nested_page_test(): void
    {
        $locale = 'nl';

        $this->createMenu();

        $pageData = [];

        for ($i = 0; $i < 3; $i++) {
            $pageData[$i] = [
                'title' => $this->faker->name,
                'content' => $this->faker->paragraph,
                'locale' => $locale,
            ];

            $this->createPage(MenuItem::PARENT_TYPE_MENU, 1, $pageData[$i]);
        }

        $orderData = [
            'order' => json_encode([
                '1' => [
                    'order' => 0,
                    'children' => [
                        '1' => [
                            'order' => '1',
                            'children' => [
                                '2' => [
                                    'order' => '0',
                                    'children' => [],
                                ],
                            ],
                        ],
                        '3' => [
                            'order' => '0',
                            'children' => [],
                        ],
                    ],
                ],
            ]),
        ];

        $this->post(route('crud.page.update-order'), $orderData);

        $this->assertDatabaseHas('liberiser_menu_items', ['id' => 1, 'order' => 1, 'menu_id' => 1, 'parent_id' => null]);
        $this->assertDatabaseHas('liberiser_menu_items', ['id' => 2, 'order' => 0, 'menu_id' => 1, 'parent_id' => 1]);
        $this->assertDatabaseHas('liberiser_menu_items', ['id' => 3, 'order' => 0, 'menu_id' => 1, 'parent_id' => null]);

        $this->assertDatabaseHas('liberiser_menu_items', ["link->{$locale}" => '/'.Str::slug($pageData[0]['title'])]);
        $this->assertDatabaseHas('liberiser_menu_items', ["link->{$locale}" => '/'.Str::slug($pageData[0]['title']).'/'.Str::slug($pageData[1]['title'])]);
        $this->assertDatabaseHas('liberiser_menu_items', ["link->{$locale}" => '/'.Str::slug($pageData[2]['title'])]);
    }

    /** @test */
    public function delete_page_test(): void
    {
        $locale = 'nl';

        $rootData = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $locale,
        ];

        $childData = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $locale,
        ];

        $this->createMenu();

        $page = $this->createPage(MenuItem::PARENT_TYPE_MENU, 1, $rootData);

        $this->createPage(MenuItem::PARENT_TYPE_MENU_ITEM, 1, $childData);

        foreach ($page->parents as $parent) {
            $this->delete(route('crud.menu.item.destroy', ['menuItem' => $parent->id]));
        }

        $this->assertSoftDeleted('liberiser_menu_items', ['id' => 1]);
        $this->assertSoftDeleted('liberiser_menu_items', ['id' => 2]);
    }

    /** @test */
    public function frontend_routes_exists_test(): void
    {
        $locale = 'en';

        $rootData = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $locale,
        ];

        $childData = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $locale,
        ];

        $this->createMenu();

        $page = $this->createPage(MenuItem::PARENT_TYPE_MENU, 1, $rootData);

        $childPage = $this->createPage(MenuItem::PARENT_TYPE_MENU_ITEM, 1, $childData);

        foreach ($page->parents as $parent) {
            $this->get('/'.$locale.$parent->getTranslation('link', $locale))->assertViewIs('LiberiserPages::pages.show');
            $this->get('/'.$locale.$parent->getTranslation('link', $locale))->assertViewIs('LiberiserPages::pages.show');
            $this->get('/wronglocale'.$parent->getTranslation('link', $locale))->assertNotFound();
        }

        $this->get('non-existing-link')->assertNotFound();
    }

    private function createPage(int $parentType, int $parentId, array $data): Page
    {
        $data = array_merge($data, ['parent_type' => $parentType, 'parent_id' => $parentId]);

        $this->post(route('crud.page.store'), $data)->assertStatus(Response::HTTP_FOUND);

        return Page::orderby('id', 'DESC')->first();
    }

    private function updatePage(Page $page, array $data): Page
    {
        $data = array_merge($data, ['id' => $page->id]);

        $this->put(route('crud.page.update', ['page' => 1]), $data)->assertStatus(Response::HTTP_FOUND);

        return $page->refresh();
    }

    private function createMenu(int $amount = 1): void
    {
        for ($i = 0; $i < $amount; $i++) {
            $inputData = [
                'name' => $this->faker->name,
            ];

            $this->post(route('crud.menu.store'), $inputData)->assertStatus(Response::HTTP_FOUND);
        }
    }
}
