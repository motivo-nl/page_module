<?php

namespace Tests\Controllers;

use Illuminate\View\View;
use Motivo\Liberiser\Pages\Models\Page;
use Motivo\Liberiser\Base\Http\Controllers\Controller;

class TestPageController extends Controller
{
    public function index(): View
    {
        return view('LiberiserPages::pages.show')->withPage(Page::where('named_route', 'testing-url')->firstOrFail());
    }
}
