<?php

namespace Tests;

use Prologue\Alerts\Facades\Alert;
use App\Http\Middleware\CheckIfAdmin;
use Backpack\Base\BaseServiceProvider;
use Backpack\CRUD\CrudServiceProvider;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Schema;
use App\Http\Middleware\VerifyCsrfToken;
use Prologue\Alerts\AlertsServiceProvider;
use Motivo\Liberiser\Base\LiberiserManager;
use Motivo\Liberiser\Seo\SeoServiceProvider;
use Motivo\Liberiser\Pages\PagesServiceProvider;
use Orchestra\Testbench\TestCase as BaseTestCase;
use Motivo\Liberiser\Base\LiberiserServiceProvider;
use Motivo\Liberiser\Redirects\RedirectsServiceProvider;

class TestCase extends BaseTestCase
{
    /** @var string */
    public static $database;

    /** @var string */
    public static $databaseUser;

    /** @var string */
    public static $databasePassword;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        if (! isset(static::$database) || ! isset(static::$databaseUser) || ! isset(static::$databasePassword)) {
            static::$database = env('LIBERISER_TESTING_DATABASE', null);
            static::$databaseUser = env('LIBERISER_TESTING_DATABASE_USER', null);
            static::$databasePassword = env('LIBERISER_TESTING_DATABASE_PASSWORD', null);

            if (file_exists(__DIR__.'/database.json')) {
                $file = file_get_contents(__DIR__.'/database.json');
                $json = json_decode($file);

                if (! isset(static::$database)) {
                    static::$database = isset($json->database) ? $json->database : 'liberiser';
                }

                if (! isset(static::$databaseUser)) {
                    static::$databaseUser = isset($json->user) ? $json->user : 'liberiser';
                }

                if (! isset(static::$databasePassword)) {
                    static::$databasePassword = isset($json->password) ? $json->password : 'root';
                }
            } else {
                if (! isset(static::$database)) {
                    static::$database = 'liberiser';
                }

                if (! isset(static::$databaseUser)) {
                    static::$databaseUser = 'liberiser';
                }

                if (! isset(static::$databasePassword)) {
                    static::$databasePassword = 'root';
                }
            }
        }

        parent::__construct($name, $data, $dataName);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->afterApplicationCreated(function () {
            $this->makeACleanSlate();

            $this->artisan('migrate', [
                '--realpath' => realpath(__DIR__.'/../vendor/motivo/liberiser-cms/src/Database/Migrations'),
            ]);

            $this->artisan('migrate', [
                '--realpath' => realpath(__DIR__.'/../src/database'),
            ]);

            $this->withoutMiddleware(VerifyCsrfToken::class);
            $this->withoutMiddleware(CheckIfAdmin::class);

            $this->loadFactoriesUsing($this->app, __DIR__.'/../database/factories');
        });

        $this->beforeApplicationDestroyed(function () {
            $this->makeACleanSlate();
        });
    }

    protected function getPackageProviders($app): array
    {
        return [
            AlertsServiceProvider::class,
            BaseServiceProvider::class,
            CrudServiceProvider::class,
            LiberiserServiceProvider::class,
            PagesServiceProvider::class,
            RedirectsServiceProvider::class,
            SeoServiceProvider::class,
        ];
    }

    protected function getPackageAliases($app)
    {
        return [
            'Alert' => Alert::class,
            'CRUD' => CrudServiceProvider::class,
            'liberiser' => LiberiserManager::class,
        ];
    }

    /**
     * Define environment setup.
     *
     * @param Application $app
     *
     * @return void
     */
    protected function getEnvironmentSetUp($app): void
    {
        $config = $app->get('config');

        $config->set('app.key', 'base64:A15nxuPO3DAzhwP5M+K87b7wMm8KE5lDNWpRuyDvuxs=');

        $config->set('app.url', 'http://test.url');

        $config->set('database.default', 'mysql');

        $config->set('database.connections.mysql.database', static::$database);
        $config->set('database.connections.mysql.username', static::$databaseUser);
        $config->set('database.connections.mysql.password', static::$databasePassword);
    }

    /**
     * Assert that the attributes of a model entry are equal to the expected array of attributes.
     *
     * @param array $expected attributes
     * @param \Illuminate\Database\Eloquent\Model $actual model
     */
    protected function assertEntryEquals($expected, $actual)
    {
        foreach ($expected as $key => $value) {
            if (is_array($value)) {
                $this->assertEquals(count($value), $actual->{$key}->count());
            } else {
                $this->assertEquals($value, $actual->{$key});
            }
        }

        $this->assertNotNull($actual->created_at);
        $this->assertNotNull($actual->updated_at);
    }

    protected function makeACleanSlate(): void
    {
        $this->truncateTestingDatabase();
    }

    protected function truncateTestingDatabase(): void
    {
        $this->artisan('migrate:fresh');
    }
}
