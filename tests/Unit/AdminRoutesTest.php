<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Support\Facades\Route;

class AdminRoutesTest extends TestCase
{
    /**
     * @dataProvider routesProvider
     * @test
     */
    public function admin_menu_routes_have_been_registered(string $routeName, bool $exists)
    {
        $this->assertEquals($exists, Route::has($routeName));
    }

    public function routesProvider(): array
    {
        return [
            [
                'route_name' => 'crud.menu.index',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.menu.store',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.menu.bulkClone',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.menu.bulkDelete',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.menu.create',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.menu.item.destroy',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.menu.reorder',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.menu.save.reorder',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.menu.search',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.menu.clone',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.menu.showDetailsRow',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.menu.listRevisions',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.menu.restoreRevision',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.menu.translateItem',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.menu.update',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.menu.destroy',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.menu.show',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.menu.edit',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.page.store',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.page.index',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.page.bulkClone',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.page.bulkDelete',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.page.create',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.page.create',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.page.save.reorder',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.page.reorder',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.page.search',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.page.update-order',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.page.clone',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.page.showDetailsRow',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.page.listRevisions',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.page.restoreRevision',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.page.translateItem',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.page.update',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.page.show',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.page.destroy',
                'exists' => true,
            ],
            [
                'route_name' => 'crud.page.edit',
                'exists' => true,
            ],
            [
                'route_name' => 'nonexistent.route.name',
                'exists' => false,
            ],
        ];
    }
}
