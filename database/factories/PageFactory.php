<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator as Faker;
use Motivo\Liberiser\Pages\Models\Page;

$factory->define(Page::class, function (Faker $faker) {
    return [
        'slug' => $faker->slug,
        'title' => $faker->name,
        'content' => $faker->text,
        'named_route' => $faker->slug,
    ];
});
