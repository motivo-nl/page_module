@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
            <small id="datatable_info_stack">{!! $crud->getSubheading() ?? ucfirst(trans('Liberiser::common.all')).' <span>'.$crud->entity_name_plural.'</span> '.trans('backpack::crud.in_the_database') !!}.</small>
        </h1>

        <ol class="breadcrumb">
            <li><a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
            <li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
            <li class="active">{{ trans('backpack::crud.list') }}</li>
        </ol>
    </section>
@endsection

@section('content')
    <div class="row m-b-10">
        <div class="col-xs-12">
            <div class="save-order hidden">
                <button class="btn btn-success js-save-order">Volgorde opslaan</button>
                <a href="javascript:window.location.reload()">Annuleer</a>
            </div>
        </div>
    </div>

        <div class="row menus-con">
            <div class="{{ $crud->getListContentClass() }}">

                @if ( $crud->buttons->where('stack', 'top')->count() )
                    <div id="bottom_top" class="hidden-print">
                        @include('crud::inc.button_stack', ['stack' => 'top'])
                        <div id="datatable_button_stack" class="pull-right text-right hidden-xs"></div>
                    </div>
                @endif

                @if ($crud->model->count())
                    <div class="box menu responsive nowrap">
                        <div class="menu-head">
                            <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
                        </div>

                        <ol class="menu-items ui-sortable">
                            @foreach($crud->getMenus() as $menu)
                                <li data-id="{{ $menu->id }}" class="menu-item responsive nowrap">
                                    <div>
                                        <a href="{{ route('crud.menu.edit', ['menu' => $menu->id]) }}" class="name">
                                            {{ $menu->name }}
                                        </a>

                                        <div class="pull-right page-options">
                                            @include('crud::inc.button_stack_pages', ['stack' => 'line', 'entry' => $menu])
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ol>
                    </div>
                @endif
            </div>
        </div>
@endsection

@push('crud_list_styles')
    <link rel="stylesheet" href="{{ asset('/css/liberiser-pages.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/backpack-crud.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/crud.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/form.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/list.css') }}">
@endpush

@push('crud_list_scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.2/jquery.ui.touch-punch.min.js"></script>
    <script src="{{ asset('/js/nestedSortable.min.js') }}"></script>
    @include('LiberiserPages::menus.js.index')
@endpush

@section('after_styles')
    @stack('crud_list_styles')
@endsection

@section('after_scripts')
    @stack('crud_list_scripts')
@endsection
