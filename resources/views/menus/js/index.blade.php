<script>
    (function() {

        $(document).on('click', '.js-save-order', saveMenuOrder);

        initSortableMenuItems();

        function initSortableMenuItems() {
            $('.menus-con .menu > ol').nestedSortable({
                forcePlaceholderSize: true,
                handle: '.page-options .js-sortable-handle',
                helper: 'clone',
                items: 'li',
                placeholder: 'placeholder',
                tabSize: 30,
                connectWith: '.menu > ol',
                tolerance: 'pointer',
                toleranceElement: '> div',
                isTree: true,
                expandedClass: 'has-submenu',
                stop: function() {
                    $('.save-order').removeClass('hidden');
                }
            }).disableSelection();
        }

        function saveMenuOrder() {
            var menu_items = {};
            var order = 1;

            $('.menu-item').each(function () {
                var menu_id = $(this).data('id');

                menu_items[menu_id] = {
                    order: order,
                };

                order += 1;
            });

            $.ajax({
                url: '{{ route('crud.menu.update-order') }}',
                type: 'post',
                data: {
                    order: JSON.stringify(menu_items),
                },
                success: function success() {
                    window.location.reload();
                },
                error: function error() {
                    new PNotify(notify.warning);
                }
            });
        }

    })();
</script>