<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>

@if ($crud->hasAccess('delete'))
    <a data-toggle="modal" data-target="#delete-modal-{{ $entry->getKey() }}" class="btn btn-xs btn-default option" data-button-type="delete-page">
        <i class="fa fa-trash"></i> <span class="text">{{ trans('backpack::crud.delete') }}</span>
    </a>
@endif

<div class="modal fade" id="delete-modal-{{ $entry->getKey() }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title visible-lg-inline" id="exampleModalLabel">{{ ucfirst(trans('Liberiser::common.modal.delete_title', ['name' => $entry->display_name])) }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{ ucfirst(trans('Liberiser::common.modal.delete', ['name' => $entry->display_name])) }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    {{ ucfirst(trans('Liberiser::common.cancel')) }}
                </button>
                <button type="button" class="btn btn-primary" onclick="deletePageEntry(this, {{ $entry->getKey() }})" data-route="{{ route('crud.menu.item.destroy', ['menuItem' => $entry->getKey()]) }}">
                    {{ ucfirst(trans('Liberiser::common.delete')) }}
                </button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        if (typeof moveModalToRoot != 'function') {
            function moveModalToRoot(id) {
                let modal = $('#delete-modal-' + id);

                modal.appendTo(document.body);
            }
        }

        moveModalToRoot({{ $entry->getKey() }});
    });

    if (typeof deletePageEntry != 'function') {
        function deletePageEntry(button, id) {
            var button = $(button);
            var deleteButton = $("a[data-target='#delete-modal-" + id + "']");
            var route = button.attr('data-route');
            var row = deleteButton.closest('li');

            $.ajax({
                url: route,
                type: 'DELETE',
                success: function(result) {
                    if (result != 1) {
                        new PNotify({
                            title: "{{ trans('backpack::crud.delete_confirmation_not_title') }}",
                            text: "{{ trans('backpack::crud.delete_confirmation_not_message') }}",
                            type: "warning"
                        });
                    } else {
                        new PNotify({
                            title: "{{ trans('backpack::crud.delete_confirmation_title') }}",
                            text: "{{ trans('backpack::crud.delete_confirmation_message') }}",
                            type: "success"
                        });

                        $('.modal').modal('hide');

                        if (row.hasClass("shown")) {
                            row.next().remove();
                        }

                        row.remove();
                    }
                },
                error: function(result) {
                    new PNotify({
                        title: "{{ trans('backpack::crud.delete_confirmation_not_title') }}",
                        text: "{{ trans('backpack::crud.delete_confirmation_not_message') }}",
                        type: "warning"
                    });
                }
            });
        }
    }
</script>