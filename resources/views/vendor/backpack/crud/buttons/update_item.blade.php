@if ($crud->hasAccess('update'))
    <a href="{{ url($crud->route.'/'.$entry->id.'/edit') }}" class="btn btn-xs option"><i class="fa fa-edit"></i>
        <span class="text">{{ trans('backpack::crud.edit') }}</span>
    </a>
@endif