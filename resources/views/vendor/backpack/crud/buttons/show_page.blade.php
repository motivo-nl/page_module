<a href="{{ url($crud->route.'/'.$entry->page->id) }}" class="btn btn-xs option">
    <i class="fa fa-eye"></i>
    <span class="text">{{ trans('backpack::crud.preview') }}</span>
</a>