@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
            <small id="datatable_info_stack">{!! $crud->getSubheading() ?? ucfirst(trans('Liberiser::common.all')).' <span>'.$crud->entity_name_plural.'</span> '.trans('backpack::crud.in_the_database') !!}.</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
            <li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
            <li class="active">{{ trans('backpack::crud.list') }}</li>
        </ol>
    </section>
@endsection

@section('content')
    <div class="row menus-con">
        <div class="{{ $crud->getListContentClass() }}">

            @if ( $crud->buttons->where('stack', 'bottom')->count() )
                <div id="bottom_buttons" class="hidden-print">
                    @include('crud::inc.button_stack', ['stack' => 'bottom'])
                    <div id="datatable_button_stack" class="pull-right text-right hidden-xs"></div>
                </div>
            @endif

            @foreach($menuItems as $menuItem)
                <div data-id="{{ $menuItem->id }}" class="box menu responsive nowrap">

                    {{-- Button to save the new order --}}
                    <div class="save-order">
                        <button class="btn btn-success js-save-order">Volgorde opslaan</button>
                        <a href="javascript:window.location.reload()">Annuleer</a>
                    </div>

                    {{-- Menu name & option to add new page --}}
                    <div class="menu-head">
                        <span class="name">{{ $menuItem->name }}</span>
                        <a href="{{ route('crud.page.create', ['parentType' => Motivo\Liberiser\Pages\Models\MenuItem::PARENT_TYPE_MENU, 'parentId' => $menuItem->id]) }}" class="pull-right">
                            <i class="fa fa-plus"></i>
                        </a>
                    </div>

                    {{-- Menu items --}}
                    <ol class="menu-items">
                        @foreach($menuItem->menuItems as $item)
                            <li data-id="{{ $item->id }}" class="{{ $item->children ? 'submenu-visible has-submenu' : '' }}">
                                <div>
                                    @if($item->children->count() > 0)
                                    <span class="toggle-submenu js-toggle-submenu">
                                        <i class="fa fa-chevron-right"></i>
                                    </span>
                                    @endif

                                    <a href="{{ route('crud.page.edit', ['page' => $item->page->id]) }}" class="name">
                                        {{ $item->page->title }}
                                    </a>

                                    <div class="pull-right page-options">
                                        @include('crud::inc.button_stack_pages', ['stack' => 'line', 'entry' => $item])
                                    </div>
                                </div>

                                @include('LiberiserPages::pages.list-children', ['menuItem' => $item])
                            </li>
                        @endforeach
                    </ol>
                </div>
            @endforeach

        </div>
    </div>
@endsection

@push('crud_list_styles')
    <link rel="stylesheet" href="{{ asset('/css/liberiser-pages.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/backpack-crud.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/crud.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/form.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/list.css') }}">
@endpush

@push('crud_list_scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.2/jquery.ui.touch-punch.min.js"></script>
    <script src="{{ asset('/js/nestedSortable.min.js') }}"></script>
    @include('LiberiserPages::pages.js.index')
@endpush

@section('after_styles')
    @stack('crud_list_styles')
@endsection

@section('after_scripts')
    @stack('crud_list_scripts')
@endsection
