<script>
    (function() {

        $('.menus-con')
            .on('click', '.js-toggle-submenu', toggleSubmenu)
            .on('click', '.js-save-order', savePageOrder);

        initSortablePages();


        function toggleSubmenu() {
            $(this).closest('li').toggleClass('submenu-visible');
        }

        function initSortablePages() {
            $('.menus-con .menu > ol').nestedSortable({
                forcePlaceholderSize: true,
                handle: '.page-options .js-sortable-handle',
                helper: 'clone',
                items: 'li',
                placeholder: 'placeholder',
                tabSize: 30,
                connectWith: '.menu > ol',
                tolerance: 'pointer',
                toleranceElement: '> div',
                isTree: true,
                expandedClass: 'has-submenu',
                stop: function(ev, data){
                    var el = $(data.item);
                    var parent_li = el.parent().closest('li');

                    if (parent_li.length == 1 && !parent_li.hasClass('has-submenu')) {
                        parent_li.addClass('has-submenu');
                    }

                    el.closest('.menu').find('.save-order').show();
                }
            }).disableSelection();
        }

        function savePageOrder() {
            var menu_items = {};
            var order = 1;
            $('.menus-con .menu').each(function () {
                var menu_id = $(this).data('id');
                menu_items[menu_id] = {
                    order: order,
                    children: getChildIds($(this)),
                };

                order += 1;
            });

            $.ajax({
                url: '{{ route('crud.page.update-order') }}',
                type: 'post',
                data: {
                    order: JSON.stringify(menu_items),
                },
                success: function success(result) {
                    window.location.reload();
                },
                error: function error(result) {
                    new PNotify(notify.warning);
                }
            });
        }

        function getChildIds(element)
        {
            var ids = {};
            var order = 1;

            $('> ol > li', element).map(function (i, e) {
                ids[$(e).data('id')] = {
                    order: order,
                    children: getChildIds($(e)),
                };

                order += 1;
            });

            return ids;
        }
    })();
</script>