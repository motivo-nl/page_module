@if ($item->children->count())
    <ol class="submenu-items list-unstyled">

    @foreach($item->children as $child)

        <li data-id="{{ $child->id }}" class="{{ $child->children->count() ? 'submenu-visible has-submenu' : '' }}">
            <div>
                <span class="toggle-submenu js-toggle-submenu">
                    <i class="fa fa-chevron-right"></i>
                </span>

                <a href="{{ route('crud.page.edit', ['page' => $child->page->id]) }}" class="name">
                    {{ $child->page->title }}
                </a>

                <div class="pull-right page-options">
                    @include('crud::inc.button_stack_pages', ['stack' => 'line', 'entry' => $child])
                </div>

            </div>

            @if ($child->children)
                @include('LiberiserPages::pages.list-children', ['item' => $child])
            @endif
        </li>
    @endforeach

    </ol>
@endif