<?php

return [
    'singular' => 'pagina',
    'plural' => 'pagina\'s',
    'name' => 'Pagina',
    'menu_label' => 'pagina\'s',
    'columns' => [
        'title' => 'titel',
        'content' => 'content',
        'named_route' => 'label',
        'controller' => 'controller koppeling',
    ],
    'fields' => [
        'title' => 'titel',
        'content' => 'content',
        'module' => 'module',
        'named_route' => 'label',
        'controller' => 'controller koppeling',
    ],
    'select_module' => 'Selecteer een module',
    'updated_order' => 'De volgorde is bijgewerkt.',
];
