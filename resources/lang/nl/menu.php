<?php

return [
    'singular' => 'menu',
    'plural' => 'menu\'s',
    'name' => 'menu',
    'menu_label' => 'menu\'s',
    'columns' => [
        'name' => 'naam',
    ],
    'fields' => [
        'name' => 'naam',
    ],
];
