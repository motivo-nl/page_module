<?php

return [
    'singular '=> 'menu',
    'plural' => 'menus',
    'name' => 'menu',
    'menu_label' => 'menus',
    'columns' => [
        'name' => 'name',
    ],
    'fields' => [
        'name' => 'name',
    ],
];
