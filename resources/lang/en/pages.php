<?php

return [
    'singular' => 'page',
    'plural' => 'pages',
    'name' => 'Page',
    'menu_label' => 'pages',
    'columns' => [
        'title' => 'title',
        'content' => 'content',
        'named_route' => 'label',
        'controller' => 'controller binding',
    ],
    'fields' => [
        'title' => 'title',
        'content' => 'content',
        'module' => 'module',
        'named_route' => 'label',
        'controller' => 'controller binding',
    ],
    'select_module' => 'Select a module',
    'updated_order' => 'The order has been updated.',
];
